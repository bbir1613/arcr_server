from flask import Flask,request,jsonify
from flask_cors import CORS
from paillier.paillier import *

from repository.user_repository import *
from repository.poll_repository import *
from repository.candidate_repository import *
from repository.vote_repository import *

from model.user import *
from model.poll import *
from model.candidate import *
from model.vote import *

app = Flask(__name__)
CORS(app)

@app.route("/", methods = ['GET','POST'])
def hello():
	return jsonify("Hello world")

@app.route("/user/create", methods = ['GET','POST'])
def create_user():
	username = request.values.get("username")
	password = request.values.get("password")
	return jsonify(UserRepository.instance().create(User(username,password)))

@app.route("/user/find", methods = ['GET','POST'])
def find_user():
	username = request.values.get("username")
	return jsonify(UserRepository.instance().find_user_by_username(username).to_json())

@app.route("/user/login", methods =['GET', 'POST'])
def login():
	try:
		username = request.values.get("username")
		password = request.values.get("password")
		print(password, username)
		user = UserRepository.instance().find_user_by_username(username) 
		token = VoteRepository.instance().find_user_token_by_id(user.id)
		if user.username == username and user.password == password:
			if token:
				return jsonify({"error": "You already voted %s" % token})
			else:		
				return jsonify(user.to_json())
		else:
			return jsonify({"error":"Invalid username or password"})
	except Exception:
			return jsonify({"error":"Invalid username or password"})

@app.route("/poll/candidates",methods = ['GET','POST'])
def get_candidates():
	poll_id = request.values.get("id")
	result = PollRepository.instance().find_candidates_by_poll_id(poll_id)
	total_votes = VoteRepository.instance().find_votes_by_pollid(poll_id)
	print(result, total_votes)
	if total_votes == 0:
		total_votes = 1
	return jsonify([{'candidate': x[0].to_json(), 
					 'votes': decrypt(x[0].privatekey,x[0].publickey,x[1]),
					 'percentage': "{0:.2f}".format(decrypt(x[0].privatekey,x[0].publickey,x[1])*100/ total_votes)
					 } for x in result])

@app.route("/vote/votes", methods = ['GET','POST'])
def get_votes():
	return jsonify([res.to_json() for res in VoteRepository.instance().find_all_votes()])

@app.route("/vote/token", methods =['GET','POST'])
def get_data_by_token():
	token = request.values.get("token")
	data = VoteRepository.instance().find_vote_date_by_token(token)
	return jsonify({"error":"token inserted is not valid"} if data == None else {"token":data})

@app.route("/vote/vote",methods = ['GET','POST'])
def vote():
	poll_id = request.values.get("pollid")
	user_id = request.values.get("userid")
	candidate_id = request.values.get("candidateid")
	vote = int(request.values.get("vote"))
	
	cand = CandidateRepository.instance().find_candidate_by_id(candidate_id)
	pub = cand.publickey
	
	vote = encrypt(pub,vote)
	
	result = VoteRepository.instance().create(user_id, poll_id, cand, vote)
	return jsonify({"result": result})

if __name__ == "__main__":
	app.run(host = '0.0.0.0')	
