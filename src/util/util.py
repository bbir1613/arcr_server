import mysql.connector
from mysql.connector import errorcode

def execute_query(query, values = None, commit = False, callback = None, error= None):
		TAG = "execute_query"
		Log(TAG, [" query : ", query, " values: ", values])
		cnx = __get_database_connection()
		cursor = cnx.cursor()
		result = None
		try:
			cursor.execute(query, values)
			if commit:
				cnx.commit()
			if callback != None:
				result = callback(cursor)
		except Exception as err:
			if error != None:
				result = error(err)
		finally:
			Log(TAG, " close connections")
			cursor.close()
			cnx.close()
		Log(TAG, [" return result ", result])
		return result

def __get_database_connection():
	TAG = "__get_database_connection";
	try:
		cnx = mysql.connector.connect(user='root',database='test')
		return cnx
	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
			Log(TAG, "Something is wrong with your user name or password")
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			Log(TAG,"Database does not exist")
		else:
			Log(TAG,str(err))

def Log(TAG,strings):
	result = ''
	if isinstance(strings,list):
		for s in strings:
			result+= str(s) 
			result+=" "
	else:
		result = strings
	print(TAG, result)
