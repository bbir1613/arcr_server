import util.util as db
from model.user import *

obj = None
USER_TABLE = "users"

class UserRepository:
	def __init__(self):
		pass

	@staticmethod
	def instance():
		global obj
		if obj != None:
			return obj
		obj = UserRepository()
		return obj

	def create(self, user):
		query = "INSERT INTO "+ USER_TABLE +" (username, password) VALUES(%s,%s)"
		values = (user.username, user.password)
		result = db.execute_query(query,values,True, lambda res:True, lambda err: False)
		return result 

	def find_user_by_username(self, username):
		query = "SELECT id, username, password FROM %s where username = '%s'" % (USER_TABLE,username)
		collect = lambda cursor: [User(username, password, id) for(id, username, password) in cursor][0]
		return db.execute_query(query, 
								callback = collect,
								error = lambda err: print(err))
