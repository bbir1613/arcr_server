import util.util as db
from model.poll import *
from paillier.paillier import *
from repository.candidate_repository import *

obj = None
POLL_TABLE = "polls"
POLL_CANDIDATE_TABLE = "canditate_polls"

class PollRepository:
	def __init__(self):
		pass

	@staticmethod
	def instance():
		global obj
		if obj != None:
			return obj
		obj = PollRepository()
		return obj

	def create(self, poll):
		query = "INSERT INTO %s(polname) VALUES('%s')" % (POLL_TABLE,poll.polname)
		return db.execute_query(query,
								commit = True, 
								callback = lambda res: res.lastrowid, 
								error = lambda err: print(err))

	def find_poll_by_pollname(self, polname):
		query = "SELECT id, polname FROM %s where polname = '%s'" % (POLL_TABLE, polname)
		collect = lambda cursor: [Poll(polname, id) for(id, polname) in cursor][0]
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print(err))

	def find_poll_by_id(self, pollid):
		query = "SELECT id, polname FROM %s where id = '%s'" % (POLL_TABLE, pollid)
		collect = lambda cursor: [Poll(polname, id) for(id, polname) in cursor][0]
		return db.execute_query(query, 
								callback = collect,
								error = lambda err: print(err))

	def find_candidates_by_poll_id(self, pollid):
		query = ("SELECT candidates.id, n, partid, lamda, niu, votes FROM %s inner join candidates on candidate_id = candidates.id INNER join publickeys on publickeys.id = candidates.publickey_id INNER join privatekeys on privatekeys.id = candidates.privatekey_id where poll_id = '%s'" 
				% (POLL_CANDIDATE_TABLE, pollid))
		collect = lambda cursor: [(Candidate(partid, PublicKey(int(n)), PrivateKey().instantiate(int(lamda),int(niu)), id = ids), int(votes)) for (ids,n ,partid,lamda, niu, votes) in cursor]
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print(err))

	def find_votes_by_candidate_id(self, candidateid, pollid):
		query = "SELECT votes FROM %s where candidate_id = '%s' and poll_id = '%s' " % (POLL_CANDIDATE_TABLE, candidateid,pollid)
		collect = lambda cursor: int(cursor.fetchone()[0])
		return db.execute_query(query,
							    callback = collect,
							    error = lambda err: print(err))

	def delete_poll_by_id(self, pollid):
		query = "DELETE FROM %s where id = '%s'" % (POLL_TABLE, pollid)
		return db.execute_query(query,
								commit= True,
								callback = lambda res: True,
								error = lambda err:  print(err))
		
	def assign_candidate_to_poll(self, pollid, candidate):
		vote = encrypt(candidate.publickey, 0);
		query = ("INSERT INTO %s(candidate_id, poll_id, votes) VALUES('%s', '%s', '%s')" 
			% (POLL_CANDIDATE_TABLE, candidate.id, pollid, vote))
		return db.execute_query(query,
								commit = True,
								callback = lambda res:True,
								error = lambda err: print(err))

	def vote_for_candidate(self, poll_id, candidate, vote):
		current_votes = self.find_votes_by_candidate_id(candidate.id,poll_id)
		vote = e_add(candidate.publickey, current_votes, vote);
		query = ("UPDATE %s SET votes = %s where poll_id = %s AND candidate_id = %s" 
				% (POLL_CANDIDATE_TABLE, vote, poll_id, candidate.id))
		return db.execute_query(query,
								commit = True,
								callback = lambda res:True,
								error = lambda err: print(err))
