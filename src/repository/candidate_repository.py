import util.util as db
from paillier.paillier import *
from model.candidate import *

obj = None
CANDIDATE_TABLE = "candidates"
PRIVATE_KEY_TABLE = "privatekeys"
PUBLIC_KEY_TABLE = "publickeys"
BITS = 40

class CandidateRepository:
	def __init__(self):
		pass

	@staticmethod
	def instance():
		global obj
		if obj != None:
			return obj
		obj = CandidateRepository()
		return obj

	def create(self, candidate):
		privateKey, publicKey = generate_keypair(BITS)
		publickey_id = self.__insert_public_key(publicKey)
		privatekey_id = self.__insert_private_key(privateKey)
		query = ("INSERT INTO %s(partid, publickey_id, privatekey_id) VALUES('%s', '%s', '%s')" 
				 % (CANDIDATE_TABLE, candidate.partid, publickey_id, privatekey_id))
		return db.execute_query(query,
								commit = True, 
								callback = lambda res:True, 
								error = lambda err: False)
		 
	def __insert_private_key(self,privateKey):
		query_privatekey = ("INSERT INTO %s(lamda, niu) VALUES('%s', '%s')" 
							% (PRIVATE_KEY_TABLE, privateKey.lamda, privateKey.niu)) 
		return db.execute_query(query_privatekey, 
								commit = True, 
								callback = lambda res : res.lastrowid,
								error = lambda err: None)

	def __insert_public_key(self,publicKey):
		query_publickey = "INSERT INTO %s(n) VALUES('%s')" % (PUBLIC_KEY_TABLE, publicKey.n)
		return db.execute_query(query_publickey, 
								commit = True, 
								callback = lambda res: res.lastrowid,
								error = lambda err: None)

	def find_candidate_by_id(self, id):
		query = ("SELECT candidates.id, n, partid FROM %s inner join %s on publickeys.id = candidates.publickey_id where candidates.id =  '%s'" 
				% (CANDIDATE_TABLE,PUBLIC_KEY_TABLE,id))
		collect = lambda cursor: [Candidate(partid, PublicKey(int(n)), id = ids) for (ids,n ,partid ) in cursor][0]
		return db.execute_query(query,
								callback = collect,
								error = lambda err: print(err))

	def find_candidate_by_name(self, partid):
		query = ("SELECT candidates.id, n, partid FROM %s inner join %s on %s.id = %s.publickey_id where %s.partid =  '%s'" 
				% (CANDIDATE_TABLE, PUBLIC_KEY_TABLE,PUBLIC_KEY_TABLE,CANDIDATE_TABLE,CANDIDATE_TABLE,partid))
		collect = lambda cursor: [Candidate(partid, PublicKey(int(n)), id = ids) for (ids,n ,partid ) in cursor][0]
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print(err))

	def find_private_key_by_candidate_id(self, id):
		query = ("SELECT  lamda, niu FROM %s inner join %s on %s.id = %s.privatekey_id where %s.id =  '%s'" 
				% (CANDIDATE_TABLE, PRIVATE_KEY_TABLE, PRIVATE_KEY_TABLE,CANDIDATE_TABLE,CANDIDATE_TABLE,id))
		collect = lambda cursor: cursor.fetchone()
		result = db.execute_query(query, 
								  callback = collect, 
								  error = lambda err: print(err))
		pk = PrivateKey()
		print(result)
		pk.set_lamda(int(result[0]))
		pk.set_niu(int(result[1]))
		return pk
