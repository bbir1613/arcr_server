import util.util as db
from model.poll import *
from paillier.paillier import *
from repository.poll_repository import *
from model.vote import *
import uuid

obj = None
VOTE_TABLE = "votes"

class VoteRepository:
	def __init__(self):
		pass

	@staticmethod
	def instance():
		global obj
		if obj != None:
			return obj
		obj = VoteRepository()
		return obj

	def create(self, user_id, poll_id, candidate, vote):
		token = uuid.uuid4().hex
		query = ("INSERT INTO %s(user_id, poll_id, token, data) VALUES('%s', '%s', '%s',NOW())" 
				% (VOTE_TABLE, user_id, poll_id, token))
		result = db.execute_query(query,
						 commit = True, 
						 callback = lambda res:True, 
						 error = lambda err: False)
		if result:
			PollRepository.instance().vote_for_candidate(poll_id,candidate,vote)
			return True
		else:
			return False

	def find_user_by_id(self, id):
		query = "SELECT COUNT(*) FROM %s where user_id = '%s'" % (VOTE_TABLE,id)
		collect = lambda cursor: cursor.fetchone()[0] == 0 
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print("find_user_by_id error", err))

	def find_votes_by_pollid(self, pollid):
		query = "SELECT COUNT(*) FROM %s where poll_id = '%s'" % (VOTE_TABLE,pollid)
		collect = lambda cursor: cursor.fetchone()[0] 
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print("find_user_by_id error", err))

	
	def find_user_token_by_id(self,id):
		query = "SELECT token FROM %s where user_id = '%s'" % (VOTE_TABLE,id)
		collect = lambda cursor: cursor.fetchone()[0] 
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print("find_user_by_id error", err))

	def find_vote_date_by_token(self, token):
		query = "SELECT data FROM %s where token = '%s'" % (VOTE_TABLE,token)
		collect = lambda cursor: cursor.fetchone()[0] 
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print("find_user_by_id error", err))


	def find_all_votes(self):
		query = "SELECT user_id, poll_id, token, data  FROM %s " % (VOTE_TABLE)
		collect = lambda cursor: [Vote(user_id, poll_id, token, data) for (user_id, poll_id, token, data) in cursor] 
		return db.execute_query(query, 
								callback = collect, 
								error = lambda err: print("find_user_by_id error", err))
