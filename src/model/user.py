'''
create table users(
    id int not null primary key AUTO_INCREMENT,
    username varchar(255) unique,
    password varchar(255)
);
'''

class User:

	def __init__(self, username, passw, id = None ):
		self.username = username
		self.password = passw
		self.id = id 

	def __repr__(self):
		return '<User: \n id: %s \n username: %s \n passwd: %s \n ' % (self.id,self.username,self.password)

	def to_json(self):
		return {'id':self.id,'username':self.username,'password': self.password}