'''
create table votes(
    id int not null primary key AUTO_INCREMENT,
    user_id int not null,
    poll_id int not null,
    token varchar(255)
);
'''

class Vote:

	def __init__(self, user_id, poll_id, token, data):
		self.user_id = user_id 
		self.poll_id = poll_id
		self.token = token
		self.data = data 

	def __repr__(self):
		return '<Vote: user_id: %s \n poll_id: %s \n token: %s \n data: %s \n' % (self.user_id, self.poll_id, self.token, self.data)

	def to_json(self):
		return {"user_id": self.user_id,"poll_id":self.poll_id,"token":self.token,"data":self.data }