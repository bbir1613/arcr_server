'''
create table candidates(
    id int not null primary key AUTO_INCREMENT,
    partid varchar(255),
    publickey text,
    privatekey text
);
'''

class Candidate:

	def __init__(self, partid, publickey = None, privatekey = None, id = None):
		self.id = id
		self.partid = partid
		self.publickey = publickey 
		self.privatekey = privatekey

	def __repr__(self):
		return '<Candidate: \n id: %s \n partid: %s \n publickey: %s \n privatekey: %s \n' % (self.id,self.partid,self.publickey, self.privatekey)

	def to_json(self):
		return {"id":self.id,"partid":self.partid,"publickey":{"n":self.publickey.n}}