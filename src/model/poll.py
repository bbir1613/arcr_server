'''
create table polls(
    id int not null primary key AUTO_INCREMENT,
    polname varchar(255)
);
'''

class Poll:

	def __init__(self,polname, id = None):
		self.id = id
		self.polname = polname

	def __repr__(self):
		return '<Poll: \n id: %s \n polname: %s \n ' % (self.id,self.polname)

	def to_json(self):
		return {"id":self.id, "polname":self.polname}