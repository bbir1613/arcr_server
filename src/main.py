from paillier.paillier import *

from repository.user_repository import *
from repository.poll_repository import *
from repository.candidate_repository import *
from repository.vote_repository import *

from model.user import *
from model.poll import *
from model.candidate import *
from model.vote import *

import hashlib

def insert_users():
	ur = UserRepository.instance()
	user = User("us","a")
	for i in range(100):
		ur.create(user)
		user.username = "us"+str(i)

def insert_candidates():
	cr = CandidateRepository.instance()
	cr.create(Candidate("candidate"))
	cr.create(Candidate("candidate1"))
	c1 = cr.find_candidate_by_name("candidate")
	c2 = cr.find_candidate_by_name("candidate1")
	return [c1, c2]	

def insert_polls(candidates):
	pr = PollRepository.instance()
	pr.create(Poll("poll1"))
	for candidate in candidates:
		pr.assign_candidate_to_poll(1,candidate)


def main():
	insert_users()
	candidates = insert_candidates()
	insert_polls(candidates)
	# passwd = hashlib.sha224("hello".encode('utf-8')).hexdigest()
	# print(str(passwd))

if __name__=="__main__":
		main()