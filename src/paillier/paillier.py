import math
import paillier.primes as primes  

class PrivateKey(object):

    def __init__(self, p = None, q = None, n=None):
        self.lamda=0
        self.niu = 0
        if p!= None or q != None or n != None:
            self.lamda = (p-1) * (q-1)
            self.niu = self.invmod(self.lamda, n)

    def set_lamda(self, lamda):
        self.lamda = lamda

    def set_niu(self, niu):
        self.niu = niu

    def instantiate(self, lamda, niu):
        self.lamda = lamda
        self.niu = niu
        return self

    def invmod(self,a, p, maxiter=1000000):
        if a == 0:
            raise ValueError('0 has no inverse mod %d' % p)
        r = a
        d = 1
        for i in range(min(p, maxiter)):
            d = ((p // r + 1) * d) % p
            r = (d * a) % p
            if r == 1:
                break
        else:
            raise ValueError('%d has no inverse mod %d' % (a, p))
        return d

    def __repr__(self):
        return '<PrivateKey: \n lamda: %s \n niu: %s>' % (self.lamda, self.niu)

class PublicKey(object):

    def __init__(self, n):
        self.n = n
        self.n_sq = n * n
        self.g = n + 1

    def __repr__(self):
        return '<PublicKey: \n n:%s>' % self.n

def generate_keypair(bits):
    p = primes.generate_prime(bits // 2)
    q = primes.generate_prime(bits // 2)
    n = p * q
    return PrivateKey(p, q, n), PublicKey(n)

def encrypt(pub, plain):    
    while True:
        r = primes.generate_prime(int(round(math.log(pub.n, 2))))
        if r > 0 and r < pub.n:
            break
    x = pow(r, pub.n, pub.n_sq)
    cipher = (pow(pub.g, plain, pub.n_sq) * x) % pub.n_sq
    return cipher

def e_add(pub, a, b):
    """Add one encrypted integer to another"""
    return a * b % pub.n_sq

def decrypt(priv, pub, cipher):
    x = pow(cipher, priv.lamda, pub.n_sq) - 1
    plain = ((x // pub.n) * priv.niu) % pub.n
    return plain

